import pydicom as dicom
import os
import numpy as np
from datetime import datetime


class data_manager():
    def __init__(self):
        self.dicom_data = 0
        self.KVP = 0
        self.slicethickness = 0
        self.slicenumber = 0
        self.ct_dimension = 0
        self.intercept = 0
        self.path = ''
        # As a parameter for the data package size.
        # This is not read out of the dicom file.
        self.package_size = 100

    def create_fileList(self):
        list = []
        for dirName, subdirList, fileList in os.walk(self.path):
            for filename in fileList:
                list.append(os.path.join(self.path, filename))
        return(list)

    def readout_dicom(self, file_list):
        # assuming that all list elements have the same dicom properties
        self.dicom_data = dicom.read_file(file_list[0])
        self.slicenumber = len(file_list)
        self.ct_dimension = (
            self.dicom_data.Rows, self.dicom_data.Columns, len(self.dicom_data)
        )
        self.intercept = self.dicom_data.RescaleIntercept

    def allocate_slice_stack(self):
        array = np.zeros(
            (self.ct_dimension[0], self.ct_dimension[1], self.package_size),
            # and the follwing line is also problematic for the MAC
            dtype = self.dicom_data.pixel_array.dtype
        )
        return array

    def load_data_save(self, file_list):
        data_stack = self.allocate_slice_stack()
        i = 0
        for filename in file_list:
            counter = i%self.package_size
            data_slice = dicom.read_file(filename)
            # here is the problem on my MAC
            data_stack[:,:,counter] = data_slice.pixel_array

            now = datetime.now()
            if counter == self.package_size - 1:
                np.save(str(data_slice.KVP)
                        + "___savedata___"
                        + str(int(i/self.package_size))
                        + "___"
                        + now.strftime('%Y-%m-%d-%H-%M-%S'), data_stack)
            elif i >= self.slicenumber - 1:
                np.save(str(data_slice.KVP)
                        + "___savedata___"
                        + str(int(i/self.package_size))
                        + "___"
                        + now.strftime('%Y-%m-%d-%H-%M-%S'), data_stack)
            i += 1

    def load_numpy_package(self, package_i):
        prefix, suffix = str(self.KVP) \
                         + "___savedata___" \
                         + str(package_i), ".npy"
        for dirName, subdirList, fileList in os.walk(os.getcwd()):
            for filename in fileList:
                if filename.endswith(suffix) and filename.startswith(prefix):
                    data = np.load(filename)
                    return(data)

    def rescale_data(self, slicenumber):
        package_i = int(slicenumber / self.package_size)
        package_slice_i = slicenumber % self.package_size
        data = self.load_numpy_package(package_i)
        return(data[:, :, package_slice_i] + self.intercept * np.ones(
            (self.ct_dimension[0], self.ct_dimension[1])
        ))

    def delete_savedata(self):
        prefix, suffix = str(self.KVP) + "___savedata___", ".npy"
        for dirName, subdirList, fileList in os.walk(os.getcwd()):
            for filename in fileList:
                if filename.endswith(suffix) and filename.startswith(prefix):
                    os.remove(filename)


class concentration_class(data_manager):
    def __init__(self):
        self.filter_top = 2900
        self.filter_down = 0

        self.muI140kV = 12.7 # I MAC @ 150keV from 01 2019
        self.muI70kV = 51.4 # I MAC from 01 2019
        # change also in GUI!
        self.muGd140kV = 18.5  # Gd MAC @ 150keV from 01 2019
        self.muGd70kV = 30.9  # Gd MAC from 01 2019

    def concentration(self, data_slice_70, data_slice_140, slicenumber):
        data_slice_70[
            np.logical_or.reduce((
                data_slice_70 > self.filter_top,
                data_slice_70 < self.filter_down,
                data_slice_140 > self.filter_top,
                data_slice_140 < self.filter_down
            ))
        ]  = 0
        data_slice_140[
            np.logical_or.reduce((
                data_slice_70 > self.filter_top,
                data_slice_70 < self.filter_down,
                data_slice_140 > self.filter_top,
                data_slice_140 < self.filter_down
            ))
        ]  = 0

        print(self.muGd70kV)

        C_I = (data_slice_70 * self.muGd140kV -
               data_slice_140 * self.muGd70kV)\
              / (self.muGd140kV * self.muI70kV -
                 self.muGd70kV * self.muI140kV)#mg/mL
        C_Gd = (data_slice_140 * self.muI70kV -
                data_slice_70 * self.muI140kV) \
               / (self.muGd140kV * self.muI70kV -
                  self.muGd70kV * self.muI140kV)#mg/mL
        now = datetime.now()
        np.save("C_I___slicenumber_"
                + str(slicenumber)
                + "___"
                + now.strftime('%d-%m-%Y-%H-%M-%S'), C_I)
        np.save("C_Gd___slicenumber_"
                + str(slicenumber)
                + "___"
                + now.strftime('%d-%m-%Y-%H-%M-%S'), C_Gd)

    def load_concentration(self, C_x, slicenumber):
        prefix, suffix = str(C_x) \
                         + "___slicenumber_" \
                         + str(slicenumber)\
                         + "___", ".npy"
        for dirName, subdirList, fileList in os.walk(os.getcwd()):
            for filename in fileList:
                if filename.endswith(suffix) and filename.startswith(prefix):
                    data = np.load(filename)
                    return(data)

    def delete_concdata(self):
        prefix1, prefix2, suffix = "C_Gd___slicenumber_",\
                                   "C_I___slicenumber_",\
                                   ".npy"
        for dirName, subdirList, fileList in os.walk(os.getcwd()):
            for filename in fileList:
                if filename.endswith(suffix) and (
                    filename.startswith(prefix1) or
                            filename.startswith(prefix2)):
                    os.remove(filename)