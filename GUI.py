from PyQt5 import QtWidgets
from PyQt5.QtGui import QIcon
import PyQt5.QtGui as QtGui
import sys
import pydicom as dicom
import pyqtgraph
import numpy

import Functions_Data_Evaluation as func

"Export bug:"
"Mit einem Rechtsklick auf die Viewboxes kann man diese zusammen oder " \
"einzeln exportieren. Dabei habe ich erst die Erfahrung gemacht, dass die " \
"Software abstürzt. Grund: Bei ImageExporter.py muss ein Integer " \
"returned werden oder kein float. Es muss folgende Zeile in ImageExporter.py" \
" geändert werden:" \
"Von" \
"bg=np.empty((self.params['width'], self.params['height'], 4), dtype=np.ubyte)"
"zu" \
"bg=np.empty((int(self.params['width']), int(self.params['height']), 4), " \
"dtype=np.ubyte)"


class App(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        # Define instances from custom classes
        self.obj70 = func.data_manager()
        self.obj140 = func.data_manager()
        self.conc_obj = func.concentration_class()

        # Define initial parameters here.
        self.slice_position = 0
        self.conc_limit1, self.conc_limit2 = 50, 50
        self.conc_limit1u, self.conc_limit2u = 10, 10
        self.spinbox_conc_upper_bound = 500

        # Define all main window widgets here.
        self.setWindowTitle("DECT concentration viewer")

        grid = QtWidgets.QGridLayout()
        # Set the central widget to fill main window with child widgets.
        centralWidget = QtWidgets.QWidget()
        centralWidget.setLayout(grid)
        self.setCentralWidget(centralWidget)

        self.status_bar = QtWidgets.QStatusBar()
        self.setStatusBar(self.status_bar)

        self.plot_widget = pyqtgraph.GraphicsView()
        self.plot_grid = pyqtgraph.GraphicsLayout()
        self.plot_widget.setCentralItem(self.plot_grid)
        grid.addWidget(self.plot_widget, 0, 0, 1, 3)

        self.create_plot_group()
        grid.addWidget(
            self.create_concentration_Gd_group(),
            1, 0
        )
        grid.addWidget(
            self.create_concentration_I_group(),
            1, 1
        )
        grid.addWidget(
            self.create_slicenumber_group(),
            1, 2
        )

        self.notify_widget = QtWidgets.QWidget()

        self.HU_widget = HU_window()
        self.HU_widget.spinbox_HU_top.setValue(self.conc_obj.filter_top)
        self.HU_widget.spinbox_HU_down.setValue(self.conc_obj.filter_down)
        self.HU_widget.spinbox_HU_top.valueChanged.connect(self.set_HU_filter)
        self.HU_widget.spinbox_HU_down.valueChanged.connect(self.set_HU_filter)

        self.EntranceWindow = OpenDialog()
        self.EntranceWindow.bt_OK.clicked.connect(self.raise_)
        self.EntranceWindow.bt_OK.clicked.connect(
            self.Compute_Concentration
        )
        self.EntranceWindow.spinbox_slicethick.valueChanged.connect(
            self.SliceThickness
        )
        self.EntranceWindow.spinbox_energy1.valueChanged.connect(self.Energy)
        self.EntranceWindow.spinbox_energy2.valueChanged.connect(self.Energy)
        self.EntranceWindow.bt_pth.clicked.connect(self.Path)

        self.EntranceWindow.FeGd_checkbox.clicked.connect(self.Set_Fe_MAC)

        # Set Menu actions here.
        exitAct = QtWidgets.QAction(QtGui.QIcon('exit.png'), 'Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(QtWidgets.qApp.quit)

        entranceAct = QtWidgets.QAction('Settings', self)
        entranceAct.setShortcut('Ctrl+u')
        entranceAct.setStatusTip('Select data parameters')
        entranceAct.triggered.connect(self.EntranceWindow.exec)

        aboutAct = QtWidgets.QAction('About', self)
        aboutAct.setShortcut('Ctrl+A')
        aboutAct.setStatusTip("Information about authors")
        aboutAct.triggered.connect(self.About_message)

        huAct = QtWidgets.QAction('HU filter', self)
        huAct.setShortcut('Ctrl+H')
        huAct.triggered.connect(self.HU_widget.show)

        # Set Menu configurations here.
        menubar = self.menuBar()
        # The following command is for MAC applications. Still have to find
        # out if that works on a MAC.
        menubar.setNativeMenuBar(False)

        fileMenu = menubar.addMenu("&File")
        fileMenu.addAction(exitAct)
        fileMenu.addAction(entranceAct)
        fileMenu.addAction(huAct)
        HelpMenu = menubar.addMenu("&Help")
        HelpMenu.addAction(aboutAct)

        self.progress_bar = QtWidgets.QProgressBar()
        self.status_bar.addPermanentWidget(self.progress_bar)

        self.show()
        self.EntranceWindow.exec()

    def create_plot_group(self):

        self.vb1 = self.plot_grid.addViewBox(
            0, 0, lockAspect='True',border='w', name="upper left"
        )
        self.vb1.invertY()
        self.vb2 = self.plot_grid.addViewBox(
            0, 1, lockAspect='True', border='w', name="upper right"
        )
        self.vb2.invertY()
        self.vb3 = self.plot_grid.addViewBox(
            1, 0, lockAspect='True', border='w', name="lower left"
        )
        self.vb3.invertY()
        self.vb4 = self.plot_grid.addViewBox(
            1, 1, lockAspect='True', border='w', name="lower right"
        )
        self.vb4.invertY()

        # Colors of concentrations
        self.pos = numpy.array([0.0, 1.0, 200])
        self.color_orange = numpy.array(
            [[0, 0, 0, 255], [255, 128, 0, 255], [0, 0, 0, 255]],
            #0,0,0,255,  255,128,0,255, 0,0,0,255
            dtype=numpy.ubyte
        )
        self.map_orange = pyqtgraph.ColorMap(self.pos, self.color_orange)
        self.lut_orange = self.map_orange.getLookupTable(
            start=0.0,
            stop=self.spinbox_conc_upper_bound,
            nPts=self.spinbox_conc_upper_bound
        )
        self.color_blue = numpy.array(
            [[0, 0, 0, 255], [0, 128, 255, 255], [0, 0, 0, 255]],
            dtype=numpy.ubyte
        )
        self.map_blue = pyqtgraph.ColorMap(self.pos, self.color_blue)
        self.lut_blue = self.map_blue.getLookupTable(
            start=0.0,
            stop=self.spinbox_conc_upper_bound,
            nPts=self.spinbox_conc_upper_bound
        )

        # Crosshair
        self.vline = pyqtgraph.InfiniteLine(angle=90, movable=False)
        self.hline = pyqtgraph.InfiniteLine(angle=0, movable=False)
        self.vb3.addItem(self.vline, ignoreBounds=True)
        self.vb3.addItem(self.hline, ignoreBounds=True)

        self.vline2 = pyqtgraph.InfiniteLine(angle=90, movable=False)
        self.hline2 = pyqtgraph.InfiniteLine(angle=0, movable=False)
        self.vb4.addItem(self.vline2, ignoreBounds=True)
        self.vb4.addItem(self.hline2, ignoreBounds=True)

        self.proxy = pyqtgraph.SignalProxy(
            self.vb3.scene().sigMouseMoved,
            rateLimit=25,
            slot=self.mouseMoved
        )

    def mouseMoved(self, evt):
        mousepoint = self.vb3.mapSceneToView(evt[0])
        self.vline.setPos(mousepoint.x())
        self.hline.setPos(mousepoint.y())
        self.vline2.setPos(mousepoint.x())
        self.hline2.setPos(mousepoint.y())
        index_y = int(mousepoint.x())
        index_x = int(mousepoint.y())
        data_Gd = self.conc_obj.load_concentration(
            "C_Gd", self.slice_position
        )
        data_I = self.conc_obj.load_concentration(
            "C_I", self.slice_position
        )
        shape = numpy.shape(data_Gd)
        if self.vb3.sceneBoundingRect().contains(evt[0]):
            if index_x >= 0 \
                and index_x <= shape[0] \
                and index_y >= 0 \
                and index_y <= shape[1]:
                self.status_bar.showMessage('    '.join([
                    str(int(data_Gd[index_x, index_y])),
                    str(int(data_I[index_x, index_y]))
                ]))

    def create_slicenumber_group(self):
        groupBox = QtWidgets.QGroupBox()

        self.spinbox_slice_label = QtWidgets.QLabel("Slice number")
        self.spinbox_slice = QtWidgets.QSpinBox()
        self.spinbox_slice.valueChanged.connect(self.Readout_Spinbox_Slice)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.spinbox_slice_label)
        vbox.addWidget(self.spinbox_slice)

        groupBox.setLayout(vbox)
        return groupBox

    def create_concentration_I_group(self):
        groupBox = QtWidgets.QGroupBox()

        self.spinbox_conc_CI_label = QtWidgets.QLabel(
            "Upper concentration bound C_I (mg/mL)"
        )
        self.spinbox_conc_CI = QtWidgets.QSpinBox()
        self.spinbox_conc_CI.setValue(self.conc_limit2)
        self.spinbox_conc_CI.valueChanged.connect(self.Readout_Spinbox_conc)

        self.spinbox_conc2_CI_label = QtWidgets.QLabel(
            "Lower concentration bound C_I (mg/mL)"
        )
        self.spinbox_conc2_CI = QtWidgets.QSpinBox()
        self.spinbox_conc2_CI.setValue(10)
        self.spinbox_conc2_CI.valueChanged.connect(self.Readout_Spinbox_conc)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.spinbox_conc_CI_label)
        vbox.addWidget(self.spinbox_conc_CI)
        vbox.addWidget(self.spinbox_conc2_CI_label)
        vbox.addWidget(self.spinbox_conc2_CI)

        groupBox.setLayout(vbox)
        return groupBox

    def create_concentration_Gd_group(self):
        groupBox = QtWidgets.QGroupBox()

        self.spinbox_conc_CGd_label = QtWidgets.QLabel(
            "Upper concentration bound C_Gd/Fe (mg/mL)"
        )
        self.spinbox_conc_CGd = QtWidgets.QSpinBox()
        self.spinbox_conc_CGd.setValue(self.conc_limit1)
        self.spinbox_conc_CGd.valueChanged.connect(self.Readout_Spinbox_conc)

        self.spinbox_conc2_CGd_label = QtWidgets.QLabel(
            "Lower concentration bound C_Gd/Fe (mg/mL)"
        )
        self.spinbox_conc2_CGd = QtWidgets.QSpinBox()
        self.spinbox_conc2_CGd.setValue(10)
        self.spinbox_conc2_CGd.valueChanged.connect(self.Readout_Spinbox_conc)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.spinbox_conc_CGd_label)
        vbox.addWidget(self.spinbox_conc_CGd)
        vbox.addWidget(self.spinbox_conc2_CGd_label)
        vbox.addWidget(self.spinbox_conc2_CGd)

        groupBox.setLayout(vbox)
        return groupBox

    def set_HU_filter(self):
        self.conc_obj.filter_down = self.HU_widget.spinbox_HU_down.value()
        self.conc_obj.filter_top = self.HU_widget.spinbox_HU_top.value()

    def Set_Fe_MAC(self):
        if self.EntranceWindow.FeGd_checkbox.isChecked():
            self.conc_obj.muGd140kV = 34.3 # Fe MAC @ 150keV from 01 2019
            self.conc_obj.muGd70kV = 36.7 # Fe MAC from 01 2019
            print("Fe activated")
        else:
            self.conc_obj.muGd140kV = 18.5  # Gd MAC @ 150keV from 01 2019
            self.conc_obj.muGd70kV = 30.9  # Gd MAC from 01 2019
            print("Gd activated")

    def Path(self):
        self.Dialog_Path = QtWidgets.QFileDialog()
        self.obj70.path = self.Dialog_Path.getExistingDirectory(
            caption = "Select directory for low energy files"
        )
        self.obj140.path = \
            self.Dialog_Path.getExistingDirectory(
            caption = "Select directory for high energy files"
        )
        # The Entrance would otherwise go into the background.
        # Dont know why yet.
        self.EntranceWindow.raise_()


    def Energy(self):
        self.obj70.KVP = self.EntranceWindow.spinbox_energy1.value()
        self.obj140.KVP = self.EntranceWindow.spinbox_energy2.value()

    def SliceThickness(self):
        self.obj70.slicethickness = \
            self.EntranceWindow.spinbox_slicethick.value()
        self.obj140.slicethickness = self.obj70.slicethickness

    def About_message(self):
        QtWidgets.QMessageBox.about(
            self, "About", "Diese Software wurde im Rahmen der Arbeitsgruppe\n"
                           "Johannes Thüring, Daniel Schmidt und Dominik "
                           "Niemietz entwickelt."
        )

    def Readout_Spinbox_Slice(self):
        self.slice_position = self.spinbox_slice.value()
        self.spinbox_slice.setRange(0, self.obj70.slicenumber - 1)

        self.img3_conc.setImage(self.conc_obj.load_concentration(
            "C_Gd",
            self.slice_position
        ))
        self.img4_conc.setImage(self.conc_obj.load_concentration(
            "C_I",
            self.slice_position
        ))

        # img_X does not remember Levels when setImage.
        self.img3_conc.setLevels([self.conc_limit1u, self.conc_limit1])
        self.img4_conc.setLevels([self.conc_limit2u, self.conc_limit2])

        self.img1.setImage(self.obj70.rescale_data(self.slice_position))
        self.img2.setImage(self.obj140.rescale_data(self.slice_position))
        self.img3.setImage(self.obj140.rescale_data(self.slice_position))
        self.img4.setImage(self.obj140.rescale_data(self.slice_position))

    def Readout_Spinbox_conc(self):
        self.conc_limit1 = self.spinbox_conc_CGd.value()
        self.conc_limit2 = self.spinbox_conc_CI.value()
        self.conc_limit1u = self.spinbox_conc2_CGd.value()
        self.conc_limit2u = self.spinbox_conc2_CI.value()

        self.img3_conc.setLevels([self.conc_limit1u, self.conc_limit1],
                                 update=True)
        self.img4_conc.setLevels([self.conc_limit2u, self.conc_limit2],
                                 update=True)

        self.spinbox_conc_CGd.setRange(
            self.conc_limit1u + 1,
            self.spinbox_conc_upper_bound
        )
        self.spinbox_conc_CI.setRange(
            self.conc_limit2u + 1,
            self.spinbox_conc_upper_bound
        )
        self.spinbox_conc2_CGd.setRange(10, self.conc_limit1 - 1)
        self.spinbox_conc2_CI.setRange(10, self.conc_limit2 - 1)

    def plot_conc(self):
        try:
            self.vb1.removeItem(self.img1)
            self.vb2.removeItem(self.img2)
            self.vb3.removeItem(self.img3)
            self.vb3.removeItem(self.img3_conc)
            self.vb4.removeItem(self.img4)
            self.vb4.removeItem(self.img4_conc)
        except:
            pass
        self.spinbox_slice.setValue(self.spinbox_slice.minimum())
        self.img1 = pyqtgraph.ImageItem(
            self.obj70.rescale_data(self.slice_position),
            axisOrder='row-major'
        )
        self.vb1.addItem(self.img1)

        self.img2 = pyqtgraph.ImageItem(
            self.obj140.rescale_data(self.slice_position),
            opacity=1,
            axisOrder='row-major'
        )
        self.vb2.addItem(self.img2)

        self.img3 = pyqtgraph.ImageItem(
            self.obj140.rescale_data(self.slice_position),
            axisOrder='row-major'
        )
        self.img3.setCompositionMode(
            QtGui.QPainter.CompositionMode_Plus
        )

        self.vb3.addItem(self.img3)
        self.img3_conc = pyqtgraph.ImageItem(
            self.conc_obj.load_concentration("C_Gd", self.slice_position),
            axisOrder='row-major'
        )
        self.img3_conc.setLevels([self.conc_limit1u, self.conc_limit1])
        self.img3_conc.setLookupTable(self.lut_orange)
        self.img3_conc.setCompositionMode(
            QtGui.QPainter.CompositionMode_Plus
        )
        self.vb3.addItem(self.img3_conc)

        self.img4 = pyqtgraph.ImageItem(
            self.obj140.rescale_data(self.slice_position),
            opacity=1,
            axisOrder='row-major'
        )
        self.img4.setCompositionMode(
            QtGui.QPainter.CompositionMode_Plus
        )
        self.vb4.addItem(self.img4)
        self.img4_conc = pyqtgraph.ImageItem(
            self.conc_obj.load_concentration("C_I", self.slice_position),
            axisOrder='row-major'
        )
        self.vb4.addItem(self.img4_conc)
        self.img4_conc.setLevels([self.conc_limit2u, self.conc_limit2])
        self.img4_conc.setLookupTable(self.lut_blue)
        self.img4_conc.setCompositionMode(
            QtGui.QPainter.CompositionMode_Plus
        )
        self.vb1.addItem(
            pyqtgraph.TextItem("%03d kV" % self.obj70.KVP)
        )
        self.vb2.addItem(
            pyqtgraph.TextItem("%03d kV" % self.obj140.KVP)
        )
        self.vb3.addItem(
            pyqtgraph.TextItem("C_Gd/Fe")
        )
        self.vb4.addItem(
            pyqtgraph.TextItem("C_I")
        )

        self.vb1.linkView(0, self.vb2)
        self.vb1.linkView(1, self.vb2)
        self.vb2.linkView(0, self.vb3)
        self.vb2.linkView(1, self.vb3)
        self.vb3.linkView(0, self.vb4)
        self.vb3.linkView(1, self.vb4)

    def Compute_Concentration(self):

        list70 = self.obj70.create_fileList()
        list140 = self.obj140.create_fileList()

        try:
            self.obj70.readout_dicom(list70)
            self.obj140.readout_dicom(list140)
        except:
            self.MessageBox = QtWidgets.QMessageBox.critical(
                self.notify_widget,
                "Error",
                "File directorie(s) do(es) not contain any DICOM data!"
            )
            return

        path_data_70_slice = dicom.read_file(list70[0])
        path_data_140_slice = dicom.read_file(list140[0])
        if self.obj70.KVP == path_data_70_slice.KVP and \
            self.obj140.KVP == path_data_140_slice.KVP and \
            self.obj70.slicethickness == path_data_70_slice.SliceThickness \
                and \
            self.obj140.slicethickness == path_data_140_slice.SliceThickness:

            self.obj70.delete_savedata()
            self.obj140.delete_savedata()
            self.conc_obj.delete_concdata()

            self.obj70.load_data_save(list70)
            self.obj140.load_data_save(list140)
        else:
            self.MessageBox = QtWidgets.QMessageBox.critical(
                self.notify_widget,
                "Error",
                "Files do not fit the chosen parameters!"
            )
            return

        self.progress_bar.setRange(0, self.obj70.slicenumber-1)
        self.conc_obj.delete_concdata()
        for slice in range(self.obj70.slicenumber):
            data_energy_70 = self.obj70.rescale_data(slice)
            data_energy_140 = self.obj140.rescale_data(slice)
            self.conc_obj.concentration(data_energy_70, data_energy_140, slice)
            self.progress_bar.setValue(slice)
        self.progress_bar.setValue(0)

        self.plot_conc()


class OpenDialog(QtWidgets.QDialog):

    def __init__(self):
        super().__init__()
        self.slicethick_down = 0
        self.slicethick_top = 10
        self.energy_down = 50
        self.energy_top = 200

        self.setWindowTitle("DECT concentration viewer")
        grid = QtWidgets.QGridLayout()
        self.setLayout(grid)

        self.bt_pth = QtWidgets.QPushButton(
            'Data directories', self
        )
        grid.addWidget(self.bt_pth, 0, 0)

        grid.addWidget(self.energy_group(), 1, 0)
        grid.addWidget(self.slicethick_group(), 2, 0)

        grid.addWidget(self.gd_or_fe(), 3, 0)

        self.bt_OK = QtWidgets.QPushButton('Compute', self)
        grid.addWidget(self.bt_OK, 4, 0)
        self.bt_OK.clicked.connect(self.hide)

    def energy_group(self):
        groupBox = QtWidgets.QGroupBox()

        self.energy1_label = QtWidgets.QLabel("Energy 1 (kV)")
        self.spinbox_energy1 = QtWidgets.QSpinBox()
        self.spinbox_energy1.setRange(self.energy_down, self.energy_top)
        self.energy2_label = QtWidgets.QLabel("Energy 2 (kV)")
        self.spinbox_energy2 = QtWidgets.QSpinBox()
        self.spinbox_energy2.setRange(self.energy_down, self.energy_top)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.energy1_label)
        vbox.addWidget(self.spinbox_energy1)
        vbox.addWidget(self.energy2_label)
        vbox.addWidget(self.spinbox_energy2)

        groupBox.setLayout(vbox)
        return groupBox

    def slicethick_group(self):
        groupBox = QtWidgets.QGroupBox()

        self.slicethick_label = QtWidgets.QLabel("Slice thickness (mm)")
        self.spinbox_slicethick = QtWidgets.QSpinBox()
        self.spinbox_slicethick.setRange(
            self.slicethick_down, self.slicethick_top
        )

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.slicethick_label)
        vbox.addWidget(self.spinbox_slicethick)

        groupBox.setLayout(vbox)
        return groupBox

    def gd_or_fe(self):
        groupBox = QtWidgets.QGroupBox()

        self.FeGd_checkbox = QtWidgets.QCheckBox("Use Fe (default: Gd)")
        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.FeGd_checkbox)

        groupBox.setLayout(vbox)
        return groupBox

class HU_window(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.HU_range_top = 3000
        self.HU_range_down = 0

        self.setWindowTitle("HU filter")
        grid = QtWidgets.QGridLayout()
        self.setLayout(grid)

        grid.addWidget(self.HU_group(), 0, 0)

        self.bt_OK = QtWidgets.QPushButton('OK', self)
        grid.addWidget(self.bt_OK, 1, 0)
        self.bt_OK.clicked.connect(self.hide)

    def HU_group(self):
        groupBox = QtWidgets.QGroupBox()

        self.HU_top_label = QtWidgets.QLabel("HU filter top")
        self.spinbox_HU_top = QtWidgets.QSpinBox()
        self.spinbox_HU_top.setRange(self.HU_range_down, self.HU_range_top)
        self.HU_down_label = QtWidgets.QLabel("HU filter down")
        self.spinbox_HU_down = QtWidgets.QSpinBox()
        self.spinbox_HU_down.setRange(self.HU_range_down, self.HU_range_top)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.HU_top_label)
        vbox.addWidget(self.spinbox_HU_top)
        vbox.addWidget(self.HU_down_label)
        vbox.addWidget(self.spinbox_HU_down)

        groupBox.setLayout(vbox)
        return groupBox

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())